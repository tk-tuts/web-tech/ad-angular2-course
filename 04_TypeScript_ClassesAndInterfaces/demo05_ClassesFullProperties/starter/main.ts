class Friend {

    // TODO: Add Accessors for the firstName property, 
    //       so that it logs to the console whenever it is set or get
    private _firstName: string

    constructor(firstName: string) {
        this.firstname = firstName;
    }

    set firstname(name: string) {
        console.log('set');
        this._firstName = name;
    }

    get firstname() {
        console.log("get");
        return this._firstName;
    }
}


let friend = new Friend("Thomas");

// When you're done, each of these lines should log to the console
friend.firstname = "Angular";
friend.firstname = "Julia";
let firstName = friend.firstname;

