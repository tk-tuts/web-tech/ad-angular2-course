function sortFriendsByName(friends) {
    var result = friends.slice(0);
    result.sort((x, y) => {
        return x.firstname.localeCompare(y.firstname);
    });
    return result;
}
class Friend {
    GetFullName(friend) {
        return friend.firstname + ' ' + friend.lastname;
    }
}
let friends = [
    { firstname: "Z", lastname: "abc" },
    { firstname: "Keerthikan", lastname: "Thurairatnam" },
];
console.log(friends);
let friendsSorted = sortFriendsByName(friends);
console.log(friendsSorted);
